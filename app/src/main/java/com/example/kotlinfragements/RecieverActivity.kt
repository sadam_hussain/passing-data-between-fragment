package com.example.kotlinfragements

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.TextView


class  RecieverActivity: AppCompatActivity() {

    lateinit var person:Person
    lateinit var tvWelcome:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reciever)
        person = intent.getParcelableExtra("person")
        d("tag","person is = ${person.name}")
        tvWelcome = findViewById(R.id.tv_welcome)
        tvWelcome.text ="welcome ${person.name}"
    }
}
