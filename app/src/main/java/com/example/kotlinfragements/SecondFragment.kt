package com.example.kotlinfragements


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.first_fragment.*

class SecondFragment : Fragment(){

    lateinit var mainActivity:MainActivity
    lateinit var name:String
    lateinit var age:String
    lateinit var etName:EditText
    lateinit var etAge:EditText
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_second, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etName = view.findViewById(R.id.et_name)
        etAge = view.findViewById(R.id.et_age)
        name= arguments?.getString("NAME").toString()
        age = arguments?.getString("AGE").toString()

        sendBtn.setOnClickListener {
            val intent = Intent(context,SerializableActivity::class.java)
            val person2 = Person2(etName.text.toString(),etAge.text.toString().toInt())
            intent.putExtra("Person2",person2)
            startActivity(intent)
        }


        secondFragment.setOnClickListener {
                mainActivity.getDataforA(etName.text.toString(),etAge.text.toString())
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity =context as MainActivity

    }

    override fun onResume() {
        super.onResume()
        et_name.setText(name)
        et_age.setText(age)
    }


    interface Frag_second
        {

            fun getDataforA(name:String,age:String)

        }



}
