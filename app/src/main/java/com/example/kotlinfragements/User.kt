package com.example.kotlinfragements

import java.io.Serializable

data class UserSerializable(var name:String,var age:Int):Serializable