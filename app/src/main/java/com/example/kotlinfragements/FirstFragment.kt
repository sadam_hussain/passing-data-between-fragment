package com.example.kotlinfragements

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.first_fragment.*
import kotlinx.android.synthetic.main.first_fragment.view.*

class FirstFragment:Fragment() {

    lateinit var name:String
    lateinit var age: String
    lateinit var etName:EditText
    lateinit var etAge:EditText
    lateinit var mainActivity: MainActivity



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,savedInstanceState: Bundle?): View?
        {
        return inflater.inflate(R.layout.first_fragment,container,false)
        }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        name= arguments?.getString("NAME").toString()
        age = arguments?.getString("AGE").toString()

        etName = view.findViewById(R.id.et_name)
        etAge = view.findViewById(R.id.et_age)
        if(name==null)
        etName.setText("")
        else  etName.setText(name)
        if(age==null)
            etAge.setText(0)
        else etAge.setText(age)
        sendBtn.setOnClickListener{
            val intent = Intent(context, RecieverActivity::class.java)
            var person =Person(etName.text.toString(),etAge.text.toString().toInt())
            intent.putExtra("person",person)
            startActivity(intent)
        }
        secondFragment.setOnClickListener {
            mainActivity.getDataB(etName.text.toString(),etAge.text.toString())
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mainActivity = context as MainActivity
    }



    interface Frag_one_interface
        {
            fun getDataB(name:String,age:String)
        }


}