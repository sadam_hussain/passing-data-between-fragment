package com.example.kotlinfragements

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.TextView

class SerializableActivity : AppCompatActivity() {


    lateinit var person2:Person2
    lateinit var tvWelcome:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_serializable)

        tvWelcome = findViewById(R.id.tv_welcome)
        person2 = intent.getSerializableExtra("Person2") as Person2
        tvWelcome.text = "welcome ${person2.name}"

    }
}
