package com.example.kotlinfragements

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), FirstFragment.Frag_one_interface ,
    SecondFragment.Frag_second {



    private val fragmentManager = supportFragmentManager
     var NAME:String ="dftet"
     var AGE:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeViews()
    }



    private fun initializeViews()
        {
            val fragmentTransaction = fragmentManager.beginTransaction()
            var fragment =FirstFragment()
            fragmentTransaction.replace(R.id.myFragments,fragment)
            fragmentTransaction.commit()

        }



    override fun getDataB(name: String, age: String) {

        var bundel = Bundle()
        bundel.putString("NAME", name)
        bundel.putString("AGE", age)

        var second = SecondFragment ()
        second.arguments=bundel
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.myFragments,second)
        fragmentTransaction.commit()

    }

    override fun getDataforA(name: String, age: String) {


        var bundel = Bundle()
        bundel.putString("NAME", name)
        bundel.putString("AGE", age)
        val fragmentTransaction = fragmentManager.beginTransaction()
        var fragment =FirstFragment()
        fragment.arguments=bundel
        fragmentTransaction.replace(R.id.myFragments,fragment)
        fragmentTransaction.commit()
    }


}
